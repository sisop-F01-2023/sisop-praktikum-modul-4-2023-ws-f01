#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#define MAX_LINE 2048

char Unzip[] = "/usr/bin/unzip";

//Membuat process
void process(char *str, char *argv[]) {
    pid_t child_id;
    int status;
    child_id = fork();
    if (child_id == 0) {
        execv(str, argv);
    } else {
        while (wait(&status) > 0);
    }
}
//Unzip file zip
void unzip_file(char *file) {
    char *argv[] = {"unzip", file, NULL};
    process(Unzip, argv);
}

typedef struct {
    int id;
    char name[50];
    int age;
    char photo[100];
    char nationality[50];
    char flag[100];
    int overall;
    int potential;
    char club[50];
} Player;

void analyze_data() {
    FILE *file = fopen("FIFA23_official_data.csv", "r");
    if (file == NULL) {
        printf("Failed to open the file.\n");
        return;
    }

    char line[MAX_LINE];
    fgets(line, MAX_LINE, file); // Skip the header line

    while (fgets(line, MAX_LINE, file)) {
        // Parsing data dari baris
        Player player;
        sscanf(line, "%d,%[^,],%d,%[^,],%[^,],%[^,],%d,%d,%[^,]",
               &player.id, player.name, &player.age, player.photo, player.nationality, 
               player.flag, &player.overall, &player.potential, player.club);

        // Tambahkan proses seleksi pemain berdasarkan kriteria
        if (player.age < 25 && player.potential > 85 && strcmp(player.club, "Manchester City") != 0) {
            printf("Name: %s\n", player.name);
            printf("Age: %d\n", player.age);
            printf("Club: %s\n", player.club);
            printf("Nationality: %s\n", player.nationality);
            printf("Potential: %d\n", player.potential);
            printf("Photo: %s\n\n", player.photo);
        }
    }
    fclose(file);
}


int main() {
    unzip_file("fifa-player-stats-database.zip");
    analyze_data();

    return 0;
}

