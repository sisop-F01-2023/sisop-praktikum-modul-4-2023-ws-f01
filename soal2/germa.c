#define FUSE_USE_VERSION 31

#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>
#include <dirent.h>
#include <time.h>
#include <stdlib.h>

#define MAX_FILES 100
#define MAX_FILENAME_LENGTH 1024

const char* productMagang_path = "/home/altriska/sisop4/soal2/nanaxgerma/src_data/germaa/products/restricted_list/productMagang";
const char* projectMagang_path = "/home/altriska/sisop4/soal2/nanaxgerma/src_data/germaa/projects/bypass_list";
const char* restrictedList_path = "/home/altriska/sisop4/soal2/nanaxgerma/src_data/germaa/projects/restricted_list";
const char* bypassList_path = "/home/altriska/sisop4/soal2/nanaxgerma/src_data/germaa/projects/bypass_list";
const char* restrictedFilePenting_path = "/home/altriska/sisop4/soal2/nanaxgerma/src_data/germaa/projects/bypass_list/restrictedFilePenting";
const char* restrictedFileDiganti_path = "/home/altriska/sisop4/soal2/nanaxgerma/src_data/germaa/projects/bypass_list/restrictedFilePenting/ourProject.txt";
const char* filePenting_path = "/home/altriska/sisop4/soal2/nanaxgerma/src_data/germaa/projects/bypass_list/filePenting";
const char* fileProject_path = "/home/altriska/sisop4/soal2/nanaxgerma/src_data/germaa/projects/bypass_list/restrictedFilePenting/ourProject.txt";
const char* fileNewProject_path = "/home/altriska/sisop4/soal2/nanaxgerma/src_data/germaa/projects/bypass_list/restrictedFilePenting/newProject.txt";
const char* logFile_path = "/home/altriska/sisop4/soal2/logmucatatsini.txt";

// Fungsi untuk mencatat aktivitas ke dalam log
void log_activity(const char *status, const char *cmd, const char *desc) {
    time_t now = time(NULL);
    struct tm *tm_info = localtime(&now);
    char timestamp[20];
    strftime(timestamp, sizeof(timestamp), "%d/%m/%Y-%H:%M:%S", tm_info);

    FILE *file = fopen(logFile_path, "a");
    if (file != NULL) {
        fprintf(file, "[%s]::[%s]::[%s]::[%s]\n", status, timestamp, cmd, desc);
        fclose(file);       
    }
}

void processDirectory(const char* path, char validFiles[][MAX_FILENAME_LENGTH], int* validCount, char invalidFiles[][MAX_FILENAME_LENGTH], int* invalidCount) {
    DIR* dir = opendir(path);
    if (dir == NULL) {
        printf("Gagal membuka direktori: %s\n", path);
        return;
    }
    
    struct dirent* entry;
    while ((entry = readdir(dir)) != NULL) {
        char filename[MAX_FILENAME_LENGTH];
        snprintf(filename, sizeof(filename), "%s/%s", pazth, entry->d_name);

        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
            continue;
        }

        if (strstr(entry->d_name, "restricted") != NULL && !strstr(entry->d_name, "bypass")) {
            strncpy(invalidFiles[(*invalidCount)++], filename, MAX_FILENAME_LENGTH);
        } else {
            strncpy(validFiles[(*validCount)++], filename, MAX_FILENAME_LENGTH);
        }

        if (entry->d_type == DT_DIR) {
            char nextPath[MAX_FILENAME_LENGTH];
            snprintf(nextPath, sizeof(nextPath), "%s/%s", path, entry->d_name);
            processDirectory(nextPath, validFiles, validCount, invalidFiles, invalidCount);
        }
    }

    closedir(dir);
}

void unzipFile(const char *zipFile, const char *unzipDir) {
    pid_t pid;
    int status;

    if (access(zipFile, F_OK) == -1) {
        printf("Zip file not found.\n");
        exit(1);
    }

    pid = fork();
    if (pid == 0) {
        freopen("/dev/null", "w", stdout); // mengarahkan output ke /dev/null
        execlp("unzip", "unzip", "-o", zipFile, "-d", unzipDir, NULL);
        exit(1);
    } else if (pid > 0) { // parent process
        while (wait(&status) != pid); // menunggu child process selesai
    } else {
        perror("fork"); // jika fork gagal, menampilkan error
        exit(1);
    }

    if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
        printf("Unzip BERHASIL.\n");
    } else {
        printf("Unzip GAGAL.\n");
    }
}
 
static int germa_mkdir(const char *path, mode_t mode)
{
    int res = mkdir(path, mode);
    if (res == -1)
        return -errno;

    char desc[100];
    snprintf(desc, sizeof(desc), "[altriska]-Create directory: %s", path);
    if (res == 0)
        log_activity("SUCCESS", "MKDIR", desc);
    else
        log_activity("FAILED", "MKDIR", desc);

    return res;
}
 
static int germa_rename(const char *oldpath, const char *newpath)
{
    int res = rename(oldpath, newpath);
    if (res == -1)
        return -errno;

    char desc[100];
    snprintf(desc, sizeof(desc), "[altriska]-Rename: %s to %s", oldpath, newpath);
    if (res == 0)
        log_activity("SUCCESS", "RENAME", desc);
    else
        log_activity("FAILED", "RENAME", desc);

    return res;
}

static int germa_unlink(const char *path)
{
    int res = unlink(path);
    if (res == -1) 
        return -errno;

    char desc[100];
    snprintf(desc, sizeof(desc), "[altriska]-Remove file: %s", path);
    if (res == 0)
        log_activity("SUCCESS", "RMFILE", desc);
    else
        log_activity("FAILED", "RMFILE", desc);

    // Panggil fungsi processDirectory untuk memproses direktori setelah file dihapus
    char validFiles[MAX_FILES][MAX_FILENAME_LENGTH];
    char invalidFiles[MAX_FILES][MAX_FILENAME_LENGTH];
    int validCount = 0;
    int invalidCount = 0;
    processDirectory(path, validFiles, &validCount, invalidFiles, &invalidCount);

    return res;
}
 
static int germa_rmdir(const char *path)
{
    int res = rmdir(path);
    if (res == -1)
        return -errno;

    if (res == 0)
        log_activity("SUCCESS", "RMDIR", path);
    else
        log_activity("FAILED", "RMDIR", "Delete Failed");
 
    return res;
}
 
static struct fuse_operations germa_oper = {
    .mkdir = germa_mkdir,
    .rename = germa_rename,
    .unlink = germa_unlink,
    .rmdir = germa_rmdir,
};

int main(int argc, char *argv[]) {
    umask(0);

    // Download ZIP file using curl
    system("curl -LOJ 'https://drive.google.com/uc?export=download&id=1iUZuYjgwcnaaEPzREPgfU6L8TYJ9633i'");

    // Delete the existing nanaxgerma directory if it exists
    system("rm -rf /home/altriska/sisop4/soal2/nanaxgerma");

    // Extract ZIP file
    unzipFile("nanaxgerma.zip", "/home/altriska/sisop4/soal2");

    // Delete the downloaded ZIP file
    system("rm nanaxgerma.zip");
    
    germa_mkdir("/home/altriska/sisop4/soal2/nanaxgerma/src_data/germaa", 0777);
    germa_rename("/home/altriska/sisop4/soal2/nanaxgerma/src_data/germaa/projects", "/home/altriska/sisop4/soal2/nanaxgerma/src_data/germaa/projects");
    germa_unlink("/home/altriska/sisop4/soal2/nanaxgerma/src_data");
    germa_rmdir("/home/altriska/sisop4/soal2");

    mode_t folderMode = 0777;

    // Membuat folder productMagang
    int result = germa_mkdir(productMagang_path, folderMode);
    if (result == 0)
        printf("Folder productMagang berhasil dibuat.\n");
    else 
        printf("Gagal membuat folder productMagang: %s\n", strerror(-result));

    // Membuat folder projectMagang
    result = germa_mkdir(projectMagang_path, folderMode);
    if (result == 0)
        printf("Folder projectMagang berhasil dibuat.\n");
    else
        printf("Gagal membuat folder projectMagang: %s\n", strerror(-result));

    // Ubah nama folder restricted_list menjadi bypass_list
    result = germa_rename(restrictedList_path, bypassList_path);
    if (result == 0)
        printf("Nama folder restrcited_list berhasil diubah.\n");
    else
        printf("Gagal mengubah nama folder restricted_list: %s\n", strerror(-result));

    // Buat folder projectMagang di dalam bypass_list
    result = germa_mkdir(projectMagang_path, folderMode);
    if (result == 0)
        printf("Folder projectMagang berhasil dibuat.\n");

     // Hapus folder projectMagang jika sudah ada
        result = germa_rmdir(projectMagang_path);
        if (result == 0)
                printf("Folder projectMagang berhasil dihapus.\n");
        else
                printf("Gagal menghapus folder projectMagang: %s\n", strerror(-result));

    // Mengubah nama folder filePenting menjadi restrictedFilePenting
    result = germa_rename(filePenting_path, restrictedFilePenting_path);
    if (result == 0)
        printf("Folder filePenting berhasil diubah menjadi restrictedFilePenting.\n");
    else
        printf("Gagal mengubah nama folder filePenting: %s\n", strerror(-result));

    // Mengubah nama file di dalam restrictedFilePenting
    result = germa_rename(fileProject_path, fileNewProject_path);
    if (result == 0)
        printf("File ourProject.txt berhasil diubah menjadi newProject.txt.\n");
    else
        printf("Gagal mengubah nama file ourProject.txt: %s\n", strerror(-result));

    // Menghapus semua "newProject" di dalam folder "restrictedFilePenting"
    char validFiles[MAX_FILES][MAX_FILENAME_LENGTH];
    char invalidFiles[MAX_FILES][MAX_FILENAME_LENGTH];
    int validCount = 0;
    int invalidCount = 0;
    processDirectory(restrictedFilePenting_path, validFiles, &validCount, invalidFiles, &invalidCount);

    // Menghapus file "newProject.txt"
    for (int i = 0; i < validCount; i++) {
        if (strcmp(validFiles[i], fileNewProject_path) == 0) {
            germa_unlink(fileNewProject_path);
        }
    }

    // Menghapus folder "restrictedFilePenting" dengan kata sihir "bypass"
    germa_unlink(restrictedFilePenting_path);

    return fuse_main(argc, argv, &germa_oper, NULL);
}
