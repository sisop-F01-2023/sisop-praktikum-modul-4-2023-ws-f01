# Laporan Resmi Praktikum Sistem Operasi Modul 4 Kelompok F01
## Anggota Kelompok
NRP | Nama |
--- | --- | 
5025211187 | Altriska Izzati Khairunnisa Hermawan |
5025211250 | Syomeron Ansell Widjaya |
5025211254 | Yusna Millaturrosyidah | 

## Daftar Isi
- [Soal 1](#soal-1)
	- [Penyelesaian soal 1](#penyelesaian-soal-1)
- [Soal 2](#soal-2)
	- [Penyelesaian soal 2](#penyelesaian-soal-2)
- [Soal 3](#soal-3)
	- [Penyelesaian soal 3](#penyelesaian-soal-3)
- [Kendala](#kendala)

### Penyelesaian soal 1
### storage.c
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#define MAX_LINE 2048

char Unzip[] = "/usr/bin/unzip";

//Membuat process
void process(char *str, char *argv[]) {
    pid_t child_id;
    int status;
    child_id = fork();
    if (child_id == 0) {
        execv(str, argv);
    } else {
        while (wait(&status) > 0);
    }
}
//Unzip file zip
void unzip_file(char *file) {
    char *argv[] = {"unzip", file, NULL};
    process(Unzip, argv);
}

typedef struct {
    int id;
    char name[50];
    int age;
    char photo[100];
    char nationality[50];
    char flag[100];
    int overall;
    int potential;
    char club[50];
} Player;

void analyze_data() {
    FILE *file = fopen("FIFA23_official_data.csv", "r");
    if (file == NULL) {
        printf("Failed to open the file.\n");
        return;
    }

    char line[MAX_LINE];
    fgets(line, MAX_LINE, file); // Skip the header line

    while (fgets(line, MAX_LINE, file)) {
        // Parsing data dari baris
        Player player;
        sscanf(line, "%d,%[^,],%d,%[^,],%[^,],%[^,],%d,%d,%[^,]",
               &player.id, player.name, &player.age, player.photo, player.nationality, 
               player.flag, &player.overall, &player.potential, player.club);

        // Tambahkan proses seleksi pemain berdasarkan kriteria
        if (player.age < 25 && player.potential > 85 && strcmp(player.club, "Manchester City") != 0) {
            printf("Name: %s\n", player.name);
            printf("Age: %d\n", player.age);
            printf("Club: %s\n", player.club);
            printf("Nationality: %s\n", player.nationality);
            printf("Potential: %d\n", player.potential);
            printf("Photo: %s\n\n", player.photo);
        }
    }
    fclose(file);
}


int main() {
    unzip_file("fifa-player-stats-database.zip");
    analyze_data();

    return 0;
}
```
Program tersebut memiliki beberapa fungsi utama, yaitu:

1. void process(char *str, char *argv[]): Fungsi ini digunakan untuk membuat proses baru menggunakan fork() dan menjalankan perintah eksekusi dengan execv(). Fungsi ini menerima argumen berupa string str yang merupakan path dari perintah eksekusi yang ingin dijalankan, dan array string argv[] yang berisi argumen-argumen untuk perintah tersebut. Fungsi ini akan melakukan fork() untuk membuat proses baru, dan dalam proses anak (child process) akan menjalankan perintah eksekusi dengan execv(). Proses induk (parent process) akan menunggu hingga proses anak selesai dengan menggunakan wait().

2. void unzip_file(char *file): Fungsi ini digunakan untuk mengekstraksi file zip. Fungsi ini menerima argumen berupa string file yang merupakan nama file zip yang ingin diekstraksi. Fungsi ini menggunakan perintah unzip dari sistem operasi untuk melakukan ekstraksi. Pada fungsi ini, dibuat array string argv[] yang berisi argumen-argumen untuk perintah unzip, kemudian fungsi process() dipanggil dengan argumen Unzip (path perintah unzip) dan argv[] untuk menjalankan perintah eksekusi.

3. void analyze_data(): Fungsi ini digunakan untuk menganalisis data dari file CSV. Fungsi ini membuka file "FIFA23_official_data.csv" dalam mode baca ("r"), kemudian membaca data dari file tersebut baris per baris menggunakan fgets(). Setiap baris yang dibaca akan diparsing menggunakan sscanf() untuk memperoleh data pemain yang terkandung dalam baris tersebut. Setelah parsing data, dilakukan seleksi pemain berdasarkan kriteria tertentu (usia, potensi, dan klub). Pemain yang memenuhi kriteria akan dicetak ke layar menggunakan printf(). Setelah selesai membaca seluruh baris data, file ditutup menggunakan fclose().

4. int main(): Fungsi ini merupakan entry point program. Pada fungsi ini, pertama kali dilakukan ekstraksi file zip dengan memanggil fungsi unzip_file() dengan argumen nama file zip yang ingin diekstraksi. Setelah ekstraksi selesai, dilakukan analisis data dengan memanggil fungsi analyze_data(). Terakhir, program mengembalikan nilai 0 yang menandakan program berjalan dengan sukses.

Jadi, secara keseluruhan, program tersebut berfungsi untuk mengekstraksi file zip dan melakukan analisis data dari file CSV yang telah diekstraksi.

### Dockerfile
```c
# Menggunakan base image Ubuntu terbaru
FROM ubuntu:latest

# Mengatur working directory menjadi /app
WORKDIR /app

# Mengupdate package list dan menginstal paket yang diperlukan
RUN apt-get update && apt-get install -y \
    gcc \                  
    zlib1g-dev \
    unzip                  

# Menyalin file storage.c ke dalam direktori /app di dalam container
COPY storage.c /app/storage.c

# Menyalin file fifa-player-stats-database.zip ke dalam direktori /app di dalam container
COPY fifa-player-stats-database.zip /app/fifa-player-stats-database.zip

# Mengkompilasi storage.c menjadi executable bernama storage dengan library zlib
RUN gcc -o storage storage.c -lz

# Menentukan perintah default yang akan dijalankan saat container dijalankan
CMD ["./storage"]

```

Dockerfile tersebut digunakan untuk membangun sebuah image Docker. Berikut adalah penjelasan langkah demi langkah dari Dockerfile tersebut:

1. `FROM ubuntu:latest`: Baris ini menentukan base image yang digunakan sebagai dasar pembangunan image. Dalam hal ini, base image yang digunakan adalah Ubuntu terbaru.

2. `WORKDIR /app`: Baris ini mengatur working directory di dalam container menjadi `/app`. Direktori `/app` akan digunakan sebagai lokasi kerja dalam container.

3. `RUN apt-get update && apt-get install -y gcc zlib1g-dev unzip`: Baris ini digunakan untuk menjalankan perintah-perintah di dalam container saat proses pembangunan image. Perintah ini akan melakukan update pada package list dan menginstal beberapa paket yang diperlukan, yaitu `gcc`, `zlib1g-dev`, dan `unzip`. `gcc` digunakan untuk mengkompilasi kode C, `zlib1g-dev` digunakan untuk mengompresi/dekompresi file menggunakan library zlib, dan `unzip` digunakan untuk mengekstraksi file zip.

4. `COPY storage.c /app/storage.c`: Baris ini menyalin file `storage.c` dari direktori lokal (host) ke dalam direktori `/app` di dalam container. File `storage.c` merupakan file yang akan dikompilasi dalam container.

5. `COPY fifa-player-stats-database.zip /app/fifa-player-stats-database.zip`: Baris ini menyalin file `fifa-player-stats-database.zip` dari direktori lokal (host) ke dalam direktori `/app` di dalam container. File ini merupakan file zip yang akan diekstraksi dalam container.

6. `RUN gcc -o storage storage.c -lz`: Baris ini menjalankan perintah untuk mengkompilasi file `storage.c` di dalam container. Perintah ini menggunakan `gcc` untuk mengkompilasi `storage.c` menjadi executable dengan nama `storage`. Opsi `-lz` digunakan untuk menghubungkan library zlib ke dalam executable.

7. `CMD ["./storage"]`: Baris ini menentukan perintah default yang akan dijalankan saat container berjalan. Perintah ini menjalankan executable `storage` yang telah dikompilasi sebelumnya.

Dengan menggunakan Dockerfile ini, kita dapat membangun image Docker yang berisi environment yang diperlukan untuk menjalankan program `storage.c` dan juga sudah mencopy file `storage.c` dan `fifa-player-stats-database.zip` ke dalam image tersebut. Setelah image dibangun, kita dapat menjalankan container dari image tersebut dengan perintah default yang sudah ditentukan.

### hubdocker.txt
```c
https://hub.docker.com/r/ansellsyomeron/storage-app
```
Berisi URL Docker Hub kode ini

### docker-compose.yml
```yaml
version: '3'
services:
  storage:
    image: ansellsyomeron/storage-app
    ports:
      - 8080:8080
    deploy:
      replicas: 5
```
File docker-compose tersebut merupakan sebuah file konfigurasi dalam format YAML yang digunakan untuk mendefinisikan dan menjalankan layanan (services) dalam lingkungan Docker. Berikut penjelasan lebih detail mengenai konfigurasi yang terdapat dalam file docker-compose tersebut:

1. version: '3': Mendefinisikan versi dari docker-compose file yang digunakan. Versi 3 adalah versi yang paling umum digunakan saat ini.

2. services: Bagian ini digunakan untuk mendefinisikan layanan yang akan dijalankan dalam lingkungan Docker. Dalam kasus ini, hanya terdapat satu layanan yang bernama "storage".

3. storage: Mendefinisikan konfigurasi untuk layanan "storage". Di dalam blok ini, terdapat beberapa konfigurasi yang didefinisikan sebagai berikut:

    - image: ansellsyomeron/storage-app: Menentukan image Docker yang akan digunakan untuk layanan "storage". Dalam hal ini, image yang digunakan adalah "ansellsyomeron/storage-app". Image ini akan digunakan untuk membuat container yang menjalankan layanan tersebut.

    - ports: Menentukan pengaturan port forwarding antara host dan container. Dalam hal ini, port 8080 di host akan diteruskan ke port 8080 di container "storage". Jadi, ketika kita mengakses port 8080 di host, permintaan akan diteruskan ke layanan "storage" di dalam container.

    - deploy: Menentukan pengaturan untuk melakukan penyebaran (deployment) layanan. Di sini, diatur bahwa akan ada 5 replika (instances) dari layanan "storage" yang akan dijalankan. Hal ini akan memungkinkan layanan "storage" berjalan pada 5 container yang terpisah, memungkinkan skala dan ketahanan yang lebih baik.

Dengan menggunakan file docker-compose ini dan menjalankan perintah docker-compose up, Docker Compose akan menggunakan konfigurasi ini untuk membangun dan menjalankan layanan "storage" dalam 5 replika dengan mengambil image "ansellsyomeron/storage-app" dan menghubungkannya ke port 8080 di host.


## Soal 2
Nana adalah peri kucing yang terampil dalam menggunakan sihir, dia bisa membuat orang lain berubah menjadi Molina. Suatu hari, dia bosan menggunakan sihir dan mencoba untuk magang di germa.dev sebagai programmer.
Agar dapat diterima sebagai karyawan magang dia diberi sebuah file .zip yang berisi folder dan file dari Germa. Kemudian, Nana harus membuat sistem manajemen folder dengan ketentuan sebagai berikut:
- Apabila terdapat file yang mengandung kata restricted, maka file tersebut tidak dapat di-rename ataupun dihapus.
- Apabila terdapat folder yang mengandung kata restricted, maka folder tersebut, folder yang ada di dalamnya, dan file yang ada di dalamnya tidak dapat di-rename ataupun dihapus.
Contoh:
/jalan/keputih/perintis/iv/sangatrestrictedloh/gang/kane.txt

Karena latar belakang Nana adalah seorang penyihir, dia ingin membuat satu kata sihir yaitu bypass yang dapat menghilangkan kedua aturan tadi. Kata sihir tersebut akan berlaku untuk folder dan file yang ada di dalamnya.

Case invalid untuk bypass:
- /jalan/keputih/perintis/iv/tidakrestrictedlohini/gang/kane.txt
- /jalan/keputih/perintis/iv/sangatrestrictedloh/gangIV/kane.txt
- /jalan/keputih/perintis/iv/sangatrestrictedloh/gang/kanelagiramai.txt

Case valid untuk bypass:
- /jalan/keputih/perintis/iv/tadirestrictedtapibypasskok/gang/kane.txt
- /jalan/keputih/perintis/iv/sangatrestrictedloh/bypassbanget/kane.txt
- /jalan/keputih/perintis/iv/sangatrestrictedloh/bypassbanget/kaneudahtutup.txt
- /jalan/keputih/perintis/iv/sangatrestrictedloh/gang/kanepakaibypass.txt

Setelah Nana paham aturan tersebut, bantu Nana untuk membuat sebuah FUSE yang bernama germa.c, yang mana dapat melakukan make folder, rename file dan folder, serta delete file dan folder. Untuk mengujinya, maka lakukan hal-hal berikut:
- Buatlah folder productMagang pada folder /src_data/germa/products/restricted_list/. Kemudian, buatlah folder projectMagang pada /src_data/germa/projects/restricted_list/. Akan tetapi, hal tersebut akan gagal.
- Ubahlah nama folder dari restricted_list pada folder /src_data/germa/projects/restricted_list/ menjadi /src_data/germa/projects/bypass_list/. Kemudian, buat folder projectMagang di dalamnya.
- Karena folder projects menjadi dapat diakses, ubahlah folder filePenting di dalam folder projects menjadi restrictedFilePenting agar secure kembali. Coba keamanannya dengan mengubah nama file yang ada di dalamnya.
- Terakhir, coba kamu hapus semua fileLama di dalam folder restrictedFileLama. Jangan lupa untuk menambahkan kata sihir pada folder tersebut agar folder tersebut dapat terhapus
- Semua kegiatan yang kalian lakukan harus tercatat dalam sebuah file logmucatatsini.txt di luar file .zip yang diberikan kepada Nana, agar pekerjaan yang kamu lakukan dapat dilaporkan kepada Germa. 

Adapun format logging yang harus kamu buat adalah sebagai berikut. 
```
[STATUS]::[dd]/[MM]/[yyyy]-[HH]:[mm]:[ss]::[CMD]::[DESC]
```
Dengan keterangan sebagai berikut.

- STATUS: SUCCESS atau FAILED.
- dd: 2 digit tanggal.
- MM: 2 digit bulan.
- yyyy: 4 digit tahun.
- HH: 24-hour clock hour, with a leading 0 (e.g. 22).
- mm: 2 digit menit.
- ss: 2 digit detik.
- CMD: command yang digunakan (MKDIR atau RENAME atau RMDIR atau RMFILE atau lainnya).
- DESC: keterangan action, yaitu:
    - [User]-Create directory x.
    - [User]-Rename from x to y.
    - [User]-Remove directory x.
    - [User]-Remove file x.

Contoh:
```
SUCCESS::17/05/2023-19:31:56::RENAME::Oky-Rename from x to y
```

Catatan: 
Keterangan untuk file .zip.
- Folder src_data adalah folder source FUSE.
- Folder dest_data adalah folder destination FUSE.

### Penyelesaian soal 2
Mulai dengan mendefinisikan library-library yanng dibutuhkan dan jangan lupa menambahkan 
```c
#define FUSE_USE_VERSION 31
```
agar FUSE dapat menggunakan versi yang sesuai.

Pertama-tama saya akan menjelaskan fungsi main terlebih dahulu untuk mengetahui alur kode ini.
```c
int main(int argc, char *argv[]) {
    umask(0);

    // Download ZIP file using curl
    system("curl -LOJ 'https://drive.google.com/uc?export=download&id=1iUZuYjgwcnaaEPzREPgfU6L8TYJ9633i'");

    // Delete the existing nanaxgerma directory if it exists
    system("rm -rf /home/altriska/sisop4/soal2/nanaxgerma");

    // Extract ZIP file
    unzipFile("nanaxgerma.zip", "/home/altriska/sisop4/soal2");

    // Delete the downloaded ZIP file
    system("rm nanaxgerma.zip");
    
    germa_mkdir("/home/altriska/sisop4/soal2/nanaxgerma/src_data/germaa", 0777);
    germa_rename("/home/altriska/sisop4/soal2/nanaxgerma/src_data/germaa/projects", "/home/altriska/sisop4/soal2/nanaxgerma/src_data/germaa/projects");
    germa_unlink("/home/altriska/sisop4/soal2/nanaxgerma/src_data");
    germa_rmdir("/home/altriska/sisop4/soal2");

    mode_t folderMode = 0777;
```
- Mengatur umask ke 0 untuk mengabaikan permission default dan memastikan bahwa semua permission yang diberikan pada file dan direktori adalah yang diminta.
- Menggunakan perintah sistem `curl` untuk mengunduh file ZIP dari URL yang diberikan.
- Menghapus direktori `nanaxgerma` jika sudah ada sebelumnya.
- Memanggil fungsi `unzipFile` untuk mengekstrak isi dari file ZIP `nanaxgerma.zip` ke direktori `/home/altriska/sisop4/soal2`.
- Menghapus file ZIP `nanaxgerma.zip` setelah selesai diekstrak.
- Membuat direktori baru dengan menggunakan fungsi `germa_mkdir` dengan path `/home/altriska/sisop4/soal2/nanaxgerma/src_data/germaa` dan permission 0777.
- Mengubah nama direktori /`home/altriska/sisop4/soal2/nanaxgerma/src_data/germaa/projects` menjadi `/home/altriska/sisop4/soal2/nanaxgerma/src_data/germaa/projects`.
- Menghapus file atau symlink `/home/altriska/sisop4/soal2/nanaxgerma/src_data`.
- Menghapus direktori `/home/altriska/sisop4/soal2`.
```c
    int result = germa_mkdir(productMagang_path, folderMode);
    if (result == 0)
        printf("Folder productMagang berhasil dibuat.\n");
    else 
        printf("Gagal membuat folder productMagang: %s\n", strerror(-result));
```
Membuat direktori baru dengan menggunakan fungsi `germa_mkdir` dengan path `productMagang_path` dan permission `folderMode`. Jika berhasil, cetak pesan keberhasilan. Jika gagal, cetak pesan kesalahan dengan menggunakan `strerror`.
```c
    result = germa_mkdir(projectMagang_path, folderMode);
    if (result == 0)
        printf("Folder projectMagang berhasil dibuat.\n");
    else
        printf("Gagal membuat folder projectMagang: %s\n", strerror(-result));
```
Membuat direktori baru dengan menggunakan fungsi `germa_mkdir` dengan path `projectMagang_path` dan permission `folderMode`. Jika berhasil, cetak pesan keberhasilan. Jika gagal, cetak pesan kesalahan dengan menggunakan `strerror`.
```c
    result = germa_rename(restrictedList_path, bypassList_path);
    if (result == 0)
        printf("Nama folder restrcited_list berhasil diubah.\n");
    else
        printf("Gagal mengubah nama folder restricted_list: %s\n", strerror(-result));
```
Mengubah nama direktori `restrictedList_path` menjadi `bypassList_path` menggunakan fungsi `germa_rename`. Jika berhasil, cetak pesan keberhasilan. Jika gagal, cetak pesan kesalahan dengan menggunakan `strerror`.
```c
    result = germa_mkdir(projectMagang_path, folderMode);
    if (result == 0)
        printf("Folder projectMagang berhasil dibuat.\n");
```
Membuat direktori baru dengan menggunakan fungsi `germa_mkdir` dengan path `projectMagang_path` dan permission `folderMode`. Jika berhasil, cetak pesan keberhasilan.
```c
    result = germa_rmdir(projectMagang_path);
    if (result == 0)
        printf("Folder projectMagang berhasil dihapus.\n");
    else
        printf("Gagal menghapus folder projectMagang: %s\n", strerror(-result));
```
Menghapus direktori `projectMagang_path` menggunakan fungsi `germa_rmdir`. Jika berhasil, cetak pesan keberhasilan. Jika gagal, cetak pesan kesalahan dengan menggunakan `strerror`.
```c
    result = germa_rename(filePenting_path, restrictedFilePenting_path);
    if (result == 0)
        printf("Folder filePenting berhasil diubah menjadi restrictedFilePenting.\n");
    else
        printf("Gagal mengubah nama folder filePenting: %s\n", strerror(-result));

```
Mengubah nama direktori `filePenting_path` menjadi `restrictedFilePenting_path` menggunakan fungsi `germa_rename`. Jika berhasil, cetak pesan keberhasilan. Jika gagal, cetak pesan kesalahan dengan menggunakan `strerror`.
```c
    result = germa_rename(fileProject_path, fileNewProject_path);
    if (result == 0)
        printf("File ourProject.txt berhasil diubah menjadi newProject.txt.\n");
    else
        printf("Gagal mengubah nama file ourProject.txt: %s\n", strerror(-result));

```
Mengubah nama file `fileProject_path` menjadi `fileNewProject_path` menggunakan fungsi `germa_rename`. Jika berhasil, cetak pesan keberhasilan. Jika gagal, cetak pesan kesalahan dengan menggunakan `strerror`.
```c
    char validFiles[MAX_FILES][MAX_FILENAME_LENGTH];
    char invalidFiles[MAX_FILES][MAX_FILENAME_LENGTH];
    int validCount = 0;
    int invalidCount = 0;
    processDirectory(restrictedFilePenting_path, validFiles, &validCount, invalidFiles, &invalidCount);

```
Mendeklarasikan array `validFiles` dan `invalidFiles` untuk menyimpan nama file yang valid dan tidak valid. Kemudian, inisialisasi `validCount` dan `invalidCount` dengan nilai 0. Selanjutnya, memanggil fungsi `processDirectory` untuk memproses direktori `restrictedFilePenting_path` dan mengisi array `validFiles` dan `invalidFiles` serta mengupdate `validCount` dan `invalidCount`.
```c
    for (int i = 0; i < validCount; i++) {
        if (strcmp(validFiles[i], fileNewProject_path) == 0) {
            germa_unlink(fileNewProject_path);
        }
    }
    germa_unlink(restrictedFilePenting_path);
    return fuse_main(argc, argv, &germa_oper, NULL);
}
```
- Menghapus file `fileNewProject_path` jika ada dalam array `validFiles`.
- Menghapus direktori `restrictedFilePenting_path`.
- Memanggil fungsi `fuse_main` dengan argumen `argc`, `argv`, `germa_oper`, dan `NULL` untuk menjalankan filesystem FUSE.

Fungsi-fungsi yang sudah disebutkan di fungsi main memiliki peran penting dalam berjalannya program ini, berikut adalah penjelasan fungsi-fungsi yang digunakan:
- Fungsi `germa_mkdir`

Fungsi ini digunakan untuk membuat direktori baru. Fungsi ini menerima path direktori yang ingin dibuat dan mode untuk izin akses pada direktori yang baru dibuat. Setelah direktori berhasil dibuat, fungsi ini akan merekam aktivitas pembuatan direktori ke dalam log dengan menggunakan fungsi `log_activity`. Jika pembuatan direktori gagal, fungsi ini akan mengembalikan kode error.

- Fungsi `germa_rename`

Fungsi ini digunakan untuk mengubah nama file atau direktori. Fungsi ini menerima path dari file atau direktori yang ingin diubah namanya, serta path baru yang dituju. Jika pengubahan nama berhasil, fungsi ini akan merekam aktivitas pengubahan nama ke dalam log menggunakan fungsi `log_activity`. Jika pengubahan nama gagal, fungsi ini akan mengembalikan kode error.

- Fungsi `germa_unlink`

Fungsi ini digunakan untuk menghapus file. Fungsi ini menerima path dari file yang ingin dihapus. Jika penghapusan file berhasil, fungsi ini akan merekam aktivitas penghapusan file ke dalam log menggunakan fungsi `log_activity`. Setelah file dihapus, fungsi ini juga akan memanggil fungsi `processDirectory` untuk memproses direktori setelah file dihapus. Jika penghapusan file gagal, fungsi ini akan mengembalikan kode error.

- Fungsi `germa_rmdir`

Fungsi ini digunakan untuk menghapus direktori. Fungsi ini menerima path dari direktori yang ingin dihapus. Jika penghapusan direktori berhasil, fungsi ini akan merekam aktivitas penghapusan direktori ke dalam log menggunakan fungsi `log_activity`. Jika penghapusan direktori gagal, fungsi ini akan mengembalikan kode error.

- Fungsi `processDirectory`

Fungsi ini digunakan untuk memproses semua file dan direktori yang ada di dalam sebuah direktori. Fungsi ini menerima path dari direktori yang akan diproses, array untuk menyimpan file yang valid, array untuk menyimpan file yang tidak valid, serta counter untuk menghitung jumlah file yang valid dan tidak valid. Fungsi ini menggunakan `opendir` untuk membuka direktori dan `readdir` untuk membaca setiap entri dalam direktori. Setiap file dan direktori akan diperiksa, dan jika file tersebut memiliki kata __"restricted"__ namun tidak mengandung kata __"bypass"__ dalam namanya, maka file tersebut akan dianggap tidak valid dan disimpan dalam array `invalidFiles`. Jika file tidak memenuhi kondisi tersebut, maka akan dianggap valid dan disimpan dalam array `validFiles`. Jika entri yang dibaca adalah direktori, fungsi ini akan memanggil dirinya sendiri secara rekursif untuk memproses direktori tersebut.

- Fungsi `log_activity`

Fungsi ini digunakan untuk mencatat aktivitas ke dalam log. Fungsi ini menerima status aktivitas (SUCCCESS/FAILED), perintah yang dilakukan, dan deskripsi aktivitas. Fungsi ini menggunakan time untuk mendapatkan timestamp saat ini dan localtime untuk mengonversi timestamp menjadi string dengan format yang sesuai. Setelah mendapatkan timestamp, status, perintah, dan deskripsi aktivitas, fungsi ini akan membuka file log menggunakan fopen dengan mode "a" untuk menambahkan ke file yang sudah ada. Setelah file log dibuka, fungsi ini akan menggunakan `fprintf` untuk menulis data ke file log dan `fclose` untuk menutup file log.

- Fungsi `unzipFile`

Fungsi ini digunakan untuk mengekstrak file ZIP menggunakan perintah unzip. Fungsi ini menerima path dari file ZIP yang akan diekstrak. Sebelum mengekstrak, fungsi ini akan memeriksa keberadaan file ZIP menggunakan access. Jika file ZIP ditemukan, fungsi ini akan menjalankan perintah unzip menggunakan system dan merekam hasil ekstraksi ke dalam log menggunakan fungsi log_activity.


## Soal 3
Dhafin adalah seorang yang sangat pemalu, tetapi saat ini dia sedang mengagumi seseorang yang dia tidak ingin seorangpun tahu. Maka dari itu, dia melakukan berbagai hal sebagai berikut.

### Penyelesaian soal 3

### Poin A
- Dhafin ingin membuat sebuah FUSE yang termodifikasi dengan source mount-nya adalah /etc yang bernama secretadmirer.c. 

```c
static const char *dirpath = "/home/yusnamillaturrosyidah/modul4/inifolderetc/sisop";
static const char *password = "modulempat";

static const char base64_table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];

    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    dp = opendir(fpath);

    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL)
    {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        char name[100];
        strcpy(name, de->d_name);
        char cur_path[1500];
        sprintf(cur_path, "%s/%s", fpath, de->d_name);

        res = (filler(buf, name, &st, 0));

        if (res != 0)
            break;
        if(de->d_type == DT_REG) {
            if(is_encoded(name)) {
                encodeBase64File(cur_path);
            }
            else if(strlen(name) <= 4) {
                convert_to_binary(name);
            }
            else {
                modify_filename(name);
            }
            changePath(cur_path, name);
        }
        else {
            if(strlen(name) <= 4) {
                convert_to_binary(name);
            }
            else {
                modify_directoryname(name);
            }
            changePath(cur_path, name);
            xmp_readdir(cur_path, buf, filler, offset, fi);
        }
    }

    closedir(dp);

    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if (strcmp(path, "/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    } else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;
    int fd = 0;

    (void)fi;

    fd = open(fpath, O_RDONLY);
    if (fd == -1)
        return -errno;

    res = pread(fd, buf, size, offset);
    if (res == -1)
        res = -errno;

    close(fd);

    return res;
}

static int xmp_open(const char *path, struct fuse_file_info *fi)
{
    (void)fi;

    if (strcmp(path, "/") == 0) {
        path = dirpath;
    } else {
        char fpath[1000];
        sprintf(fpath, "%s%s", dirpath, path);
        if (access(fpath, F_OK) == 0) {
            char input_password[100];
            printf("Input Password: ");
            fflush(stdout);
            fgets(input_password, sizeof(input_password), stdin);

            size_t len = strlen(input_password);
            if (len > 0 && input_password[len - 1] == '\n')
                input_password[len - 1] = '\0';

            while (strcmp(input_password, password) != 0) {
                printf("Incorret Password, Try Again!.\nInput password: ");
                fflush(stdout);
                fgets(input_password, sizeof(input_password), stdin);

                len = strlen(input_password);
                if (len > 0 && input_password[len - 1] == '\n')
                    input_password[len - 1] = '\0';
            }
        }
    }

    return 0;
}

static int xmp_mkdir(const char *path, mode_t mode)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    char modified_name[256];
    strcpy(modified_name, path);
    for (int i = 0; modified_name[i] != '\0'; i++) {
        modified_name[i] = toupper(modified_name[i]);
    }

    int res = mkdir(fpath, mode);
    if (res == -1) {
        return -errno;
    }

    return 0;
}

static int xmp_rename(const char *from, const char *to)
{
    char fpath_from[1000];
    char fpath_to[1000];
    sprintf(fpath_from, "%s%s", dirpath, from);
    sprintf(fpath_to, "%s%s", dirpath, to);

    char modified_name[256];
    strcpy(modified_name, to);
    for (int i = 0; modified_name[i] != '\0'; i++) {
        modified_name[i] = toupper(modified_name[i]);
    }

    int res = rename(fpath_from, fpath_to);
    if (res == -1) {
        return -errno;
    }

    return 0;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .open = xmp_open,
    .mkdir = xmp_mkdir,
    .rename = xmp_rename,
};

int main(int argc, char *argv[])
{
    umask(0);

    int fuse_stat = fuse_main(argc, argv, &xmp_oper, NULL);

    return fuse_stat;
}
```

Untuk potongan kode diatas kurang lebih menggunakan template yang sama dengan modul, akan tetapi juga mencakup beberapa fungsi yang digunakan untuk operasi-operasi tambahan terkait dengan pengolahan nama file dan direktori. Kode diatas adalah implementasi dari FUSE yang dimodifikasi untuk beberapa operasi yang dilakukan seperti mengenkripsi file dengan base64, mengubah nama file menjadi biner, dan mengubah nama direktori menjadi huruf kapital.

### Poin B 
- Lalu, untuk menutupi rahasianya, Dhafin ingin semua direktori dan file yang berada pada direktori yang diawali dengan huruf L, U, T, dan H akan ter-encode dengan Base64.  Encode berlaku rekursif (berlaku untuk direktori dan file yang baru di-rename atau pun yang baru dibuat).

```c
void encodeBase64File(const char *filename) {
    FILE *file = fopen(filename, "rb");
    if (file == NULL) {
        printf("Error : Failed to open the file: %s\n", filename);
        return;
    }

    fseek(file, 0, SEEK_END);
    long file_size = ftell(file);
    fseek(file, 0, SEEK_SET);

    uint8_t *file_content = (uint8_t *)malloc(file_size);
    if (file_content == NULL) {
        printf("Error : Memory allocation failed\n");
        fclose(file);
        return;
    }

    size_t bytes_read = fread(file_content, 1, file_size, file);
    fclose(file);

    if (bytes_read != file_size) {
        printf("Error : Failed to read file: %s\n", filename);
        free(file_content);
        return;
    }

    uint8_t input[3], output[4];
    size_t input_len = 0;
    size_t output_len = 0;
    size_t i = 0;
    
    file = fopen(filename, "wb");
    if (file == NULL) {
        printf("Error : Failed to open file for writing: %s\n", filename);
        free(file_content);
        return;
    }

    while (i < file_size) {
        input[input_len++] = file_content[i++];
        if (input_len == 3) {
            output[0] = base64_table[input[0] >> 2];
            output[1] = base64_table[((input[0] & 0x03) << 4) | ((input[1] & 0xF0) >> 4)];
            output[2] = base64_table[((input[1] & 0x0F) << 2) | ((input[2] & 0xC0) >> 6)];
            output[3] = base64_table[input[2] & 0x3F];

            fwrite(output, 1, 4, file);

            input_len = 0;
            output_len += 4;
        }
    }

    if (input_len > 0) {
        for (size_t j = input_len; j < 3; j++) {
            input[j] = 0;
        }

        output[0] = base64_table[input[0] >> 2];
        output[1] = base64_table[((input[0] & 0x03) << 4) | ((input[1] & 0xF0) >> 4)];
        output[2] = base64_table[((input[1] & 0x0F) << 2) | ((input[2] & 0xC0) >> 6)];
        output[3] = base64_table[input[2] & 0x3F];

        fwrite(output, 1, input_len + 1, file);

        for (size_t j = input_len + 1; j < 4; j++) {
            fputc('=', file);
        }

        output_len += 4;
    }

    fclose(file);
    free(file_content);

    printf("Encoding completed successfully. file: %s\n", filename);
}

bool is_encoded(const char *name) {
    char first_char = name[0];
    if (first_char == 'l' || first_char == 'L' || first_char == 'u' || first_char == 'U' || first_char == 't' || first_char == 'T' || first_char == 'h' || first_char == 'H') {
        return true;
    }
    return false;
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];

    sprintf(fpath, "%s%s", dirpath, path);

    res = lstat(fpath, stbuf);

    if (res == -1)
        return -errno;

    return 0;
}

void modify_filename(char *name)
{
    int i = 0;
    while (name[i] != '\0')
    {
        name[i] = tolower(name[i]);
        i++;
    }
}

void modify_directoryname(char *name)
{
    int i = 0;
    while (name[i] != '\0')
    {
        name[i] = toupper(name[i]);
        i++;
    }
}


void convert_to_binary(char *name)
{
    int len = strlen(name);
    char *temp = (char *)malloc((8 * len + len) * sizeof(char)); 

    int index = 0; 

    for (int i = 0; i < len; i++)
    {
        char ch = name[i];

        for (int j = 7; j >= 0; j--)
        {
            temp[index++] = (ch & 1) + '0'; 
            ch >>= 1;                       
        }

        if (i != len - 1)
        {
            temp[index++] = ' '; 
        }
    }

    temp[index] = '\0';

    strcpy(name, temp);
    free(temp);        
}

void changePath(char *str1, char *str2) {
    char *slash = strrchr(str1, '/');
    char temp[100];
    strcpy(temp, str1);
    if (slash != NULL) {
        ++slash; 
        strcpy(slash, str2);
        rename(temp, str1);
    }
```

Fungsi `encodeBase64File` digunakan untuk membaca konten file biner dan mengenkripsinya menjadi format Base64. `is_encoded` berfungsi untuk memeriksa apakah suatu nama file terenkripsi dengan menggunakan aturan tertentu. Jika huruf pertama dari nama file adalah `'l', 'L', 'u', 'U', 't', 'T', 'h', atau 'H'`, maka dianggap terenkripsi dan fungsi akan mengembalikan nilai `true`, jika tidak, maka dianggap tidak terenkripsi dan fungsi akan mengembalikan nilai `false`. Secara keseluruhan potongan kode diatas mengimplementasikan beberapa operasi terkait modifikasi nama file dan direktori dan enkripsi Base64.

### Poin C 
- Untuk membedakan antara file dan direktori, Dhafin ingin semua format penamaan file menggunakan lowercase sedangkan untuk direktori semua harus uppercase. Untuk nama file atau direktori yang hurufnya kurang dari atau sama dengan empat, maka ubah namanya menggunakan data format binary yang didapat dari ASCII code masing-masing karakter.

```c
void modify_filename(char *name)
{
    int i = 0;
    while (name[i] != '\0')
    {
        name[i] = tolower(name[i]);
        i++;
    }
}

void modify_directoryname(char *name)
{
    int i = 0;
    while (name[i] != '\0')
    {
        name[i] = toupper(name[i]);
        i++;
    }
}


void convert_to_binary(char *name)
{
    int len = strlen(name);
    char *temp = (char *)malloc((8 * len + len) * sizeof(char)); 

    int index = 0; 

    for (int i = 0; i < len; i++)
    {
        char ch = name[i];

        for (int j = 7; j >= 0; j--)
        {
            temp[index++] = (ch & 1) + '0'; 
            ch >>= 1;                       
        }

        if (i != len - 1)
        {
            temp[index++] = ' '; 
        }
    }

    temp[index] = '\0';

    strcpy(name, temp);
    free(temp);        
}
```

Potongan Kode diatas terdiri dari tiga fungsi, yaitu `modify_filename`, `modify_directoryname`, dan `convert_to_binary`.
Fungsi `modify_filename` digunakan untuk memodifikasi nama file menjadi huruf kecil semua. Fungsi `modify_directoryname` digunakan untuk memodifikasi nama direktori menjadi huruf besar semua. Kedua fungsi ini melakukan iterasi karakter per karakter pada string `name` dan mengubahnya menjadi huruf kecil atau huruf besar sesuai dengan kebutuhan.

Fungsi `convert_to_binary` digunakan untuk mengonversi setiap karakter dalam string `name` menjadi representasi biner. Setiap karakter akan diubah menjadi representasi biner 8-bit menggunakan angka 0 dan 1. Hasil konversi akan disimpan dalam string `temp` yang dialokasikan secara dinamis. Setelah selesai, string `name` akan diperbarui dengan hasil konversi biner, dan memori yang dialokasikan untuk `temp` akan dibebaskan.

### Poin D 
- Setiap ingin membuka file, maka harus memasukkan password terlebih dahulu (set password tidak ditentukan a.k.a terserah).

```c
static int xmp_open(const char *path, struct fuse_file_info *fi)
{
    (void)fi;

    if (strcmp(path, "/") == 0) {
        path = dirpath;
    } else {
        char fpath[1000];
        sprintf(fpath, "%s%s", dirpath, path);
        if (access(fpath, F_OK) == 0) {
            char input_password[100];
            printf("Input Password: ");
            fflush(stdout);
            fgets(input_password, sizeof(input_password), stdin);

            size_t len = strlen(input_password);
            if (len > 0 && input_password[len - 1] == '\n')
                input_password[len - 1] = '\0';

            while (strcmp(input_password, password) != 0) {
                printf("Incorret Password, Try Again!.\nInput password: ");
                fflush(stdout);
                fgets(input_password, sizeof(input_password), stdin);

                len = strlen(input_password);
                if (len > 0 && input_password[len - 1] == '\n')
                    input_password[len - 1] = '\0';
            }
        }
    }

    return 0;
}
```

Potongan kode diatas merupakan implementasi dari fungsi `xmp_open` dalam program yang menggunakan FUSE untuk mengatur sistem file. Fungsi ini dipanggil ketika terdapat permintaan untuk membuka file. Ketika user membuka file maka harus menginputkan password terlebih dahulu seperti yang sudah dituliskan didalam program.

### Poin E
- Selanjutnya, Dhafin ingin melakukan mounting FUSE tersebut di dalam Docker Container menggunakan Docker Compose (gunakan base image Ubuntu Bionic ). Ketentuannya, terdapat dua buah container sebagai berikut.
	- Container bernama Dhafin akan melakukan mount FUSE yang telah dimodifikasi tersebut. 
	- Container bernama Normal akan melakukan mount yang hanya menampilkan /etc yang normal tanpa ada modifikasi apapun. 

```yml
version: "3"

services:
    dhafin:
        image: ubuntu:bionic
        container_name: Dhafin
        command: tail -f /dev/null
        volumes:
            - /home/yusnamillaturrosyidah/modul4/inifolderetc/sisop:/dhafin

    normal:
        image: ubuntu:bionic
        container_name: Normal
        command: tail -f /dev/null
        volumes:
            - /home/yusnamillaturrosyidah/modul4/etc:/normal
```
Potongan kode diatas merupakan Docker Compose file yang mendefinisikan dua services/layanan yaitu `dhafin` dan `normal`. Setiap services menggunakan image `ubuntu:bionic` dan memiliki nama container yang berbeda. 

Layanan `dhafin` dikonfigurasi dengan perintah `tail -f /dev/null`, yang membuat container tetap berjalan tanpa melakukan operasi apa pun. Layanan ini juga mengaitkan volume `/home/yusnamillaturrosyidah/modul4/inifolderetc/sisop` dengan direktori dalam container `/dhafin`. Ini berarti direktori lokal akan tersedia di dalam container dengan direktori tujuan `/dhafin`.

Sedangkan, Layanan `normal` juga dikonfigurasi dengan perintah `tail -f /dev/null` dan memiliki volume yang dihubungkan dari `/home/yusnamillaturrosyidah/modul4/etc` ke direktori dalam container `/normal`.

### Poin F 
- Kalian masih penasaran siapa yang dikagumi Dhafin? 🥰😍😘😚🤗😻💋💌💘💝💖💗 Coba eksekusi image fhinnn/sisop23 deh, siapa tahu membantu. 🙂

Untuk menyelesaikan soal F ini cukup menjalankan perintah `fhinnn/sisop23` diterminal, kemudian akan muncul output seperti berikut :


Setelah dilihat dan dicek ternyata ada perbedaan isi  file yang ada di inifolderetc/sisop dan tidak ada di file /etc yaitu file `kyaknysihflag`. Setelah dibuka ternyata muncul file .txt seperti berikut :

## Kendala
- Masih belum terbiasa dengan pengimplementasian __Docker__ dan __FUSE__ sehingga terdapat kebingungan dalam mengerjakan soal.
